package operadores;

public class Condicional {

	public static void main(String[] args) {
		boolean x = true;
		boolean y = false;
		
		System.out.println(x&&y);
		System.out.println(x||y);
		System.out.println(x^y);
		System.out.println(!x);
		System.out.println(!y);
		

		System.out.println("\n");
		//System.out.println(false&&false);
		//System.out.println(false&&true);
		System.out.println(true&&false);
		System.out.println(true&&true);
		
		System.out.println("\n");
		System.out.println(false||false);
		System.out.println(false||true);
		//System.out.println(true||false);
		//System.out.println(true||true);
		
		System.out.println("\n");
		System.out.println(false^false);
		System.out.println(false^true);
		System.out.println(true^false);
		System.out.println(true^true);
		
		System.out.println("\n");
		System.out.println(!false);
		System.out.println(!true);
		
		
		
				

	}

}
