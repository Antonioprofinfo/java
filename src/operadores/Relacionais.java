package operadores;

public class Relacionais {

	public static void main(String[] args) {
		
		int a = 97; //decimal
		int b = 'a'; // Converte 'a' em unicode para 97
				
		System.out.println('\u0061'); //Unicode letra 'a' em hexadecimal
		
		
		System.out.println(a == b);
		System.out.println(a > b);
		System.out.println(a >= b);
		System.out.println(a < b);
		System.out.println(a <= b);
		System.out.println(a != b);
		
		double nota = 7;
		boolean bomComportamento = true;
		boolean passouPorMedia = nota >= 7;		
		boolean temDesconto = bomComportamento && passouPorMedia;
		System.out.println("Tem desconto " +temDesconto);
	
	}

}
