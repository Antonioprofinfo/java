package operadores;

public class Ternario {

	public static void main(String[] args) {
		//Operador Tern�rio
		double media = 5;
		String resultadoA = media >=7 ? "Aprovado":"Reprovado";
		String resultadoB = media >=7 ? "Aprovado":media>=5 ? "Recupera��o":"Reprovado";
		
		System.out.println("O aluno est� " + resultadoA);
		System.out.println("O aluno est� " + resultadoB);
		
		
		double nota = 6;
		boolean bomComportamento = true;
		boolean passouPorMedia = nota >= 7;		
		boolean temDesconto = bomComportamento && passouPorMedia;
		String resultado = temDesconto ? "Sim." : "N�o.";
		
		System.out.println("Tem desconto? " +resultado);
		System.out.printf("Tem desconto? %s" , resultado);
		
		
	}

}
