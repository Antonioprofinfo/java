package operadores;

import java.util.Scanner;

public class TipoStringEquals {

	public static void main(String[] args) {
		System.out.println("2"=="2");
		
		String s = new String ("2");
		System.out.println("2"== s); //false
		System.out.println("2".equals(s)); //true
		
		Scanner entrada = new Scanner (System.in);
		
		
		System.out.println("Digite um 2 com espa�o");
		String s2 = entrada.nextLine(); //N�o remove os espa�os em branco ao digitar
		System.out.println(s2);
		System.out.println("2".equals(s2));
		System.out.println("2".equals(s2.trim()));  //O trim remove os espa�os
		
		System.out.println("Digite um 2 com espa�o");
		String s1 = entrada.next(); //Remove os espa�os em branco ao digitar
		System.out.println(s1);
		System.out.println("2".equals(s1));
		System.out.println("2".equals(s1.trim())); //O trim remove os espa�os
		
		entrada.close();
		
		
		
		

	}

}
