package OO.encapsulamento.casaB;

import OO.encapsulamento.CasaA.Ana;

public class Julia {
	
	Ana sogra = new Ana(); 
	
		void testeAcessos() {
			//System.out.println(sogra.segredo); //Privado
			//System.out.println(sogra.facoDentroDeCasa); //Visibilidade de Pacote
			//System.out.println(sogra.formaDeFalar); //protected -> transmitido por heran�a -> Se instanciar n�o funciona.
			System.out.println(sogra.todosSabem); // public
			
			
		}
}
