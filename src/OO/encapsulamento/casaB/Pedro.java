package OO.encapsulamento.casaB;

import OO.encapsulamento.CasaA.Ana; //Importar de outro Pacote

public class Pedro extends Ana{
	
//Ana mae = new Ana(); N�o � necess�rio criar a instancia.
	
	void testeAcessos() {
		//System.out.println(mae.segredo); //Privado
		//System.out.println(mae.facoDentroDeCasa); Visibilidade de Pacote
		//System.out.println(mae.formaDeFalar); //protected -> transmitido por heran�a -> Se instanciar n�o fiunciona.
		//System.out.println(mae.todosSabem); // public
		
		
		//System.out.println(segredo);
		//System.out.println(facoDentroDeCasa);
		System.out.println(formaDeFalar); //protected -> transmitido por heran�a
		System.out.println(todosSabem); // public -> transmitido por heran�a
		
	}
	

}
