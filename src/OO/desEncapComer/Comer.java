package OO.desEncapComer;

public class Comer {
	
	public static void main(String[] args) {
		Pessoa p1 = new Pessoa(99.6);
		
		Arroz parte1 = new Arroz(0.25);
		Feijao parte2 = new Feijao(0.12);
		Sorvete parte3 = new Sorvete(0.1);
		
		Comida parte4 = new Arroz(0.25);
		
		//Desixou de funcionar ap�s a 
		//implemneta��o da abstra��o na classe comida
		// N�o pode ser instanciado
		//Comida parte5 = new Comida(0.25);
		
		System.out.println(p1.getPeso());
		
		p1.comer(parte1);
		System.out.println(p1.getPeso());
		
		p1.comer(parte2);
		System.out.println(p1.getPeso());
		
		p1.comer(parte3);
		System.out.println(p1.getPeso());
		
		p1.comer(parte4);
		System.out.println(p1.getPeso());
		
		//p1.comer(parte5);
		//System.out.println(p1.getPeso());
	}
	
	
}
