package OO.abstracao;

public abstract class Mamifero extends Animal {

	public abstract String mamar();
	
	@Override
	public final String mover() { // Final -> n�o pode ser subscrito.
		return "Anda com as patas";
	}
}
