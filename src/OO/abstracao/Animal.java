package OO.abstracao;

public abstract class Animal {
	public String respirar() {
		return "Usando o CO2";
	}
	
	public abstract String mover();
	
	public abstract String mamar();
}
