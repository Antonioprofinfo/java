package OO.composicao;

public class CompraTest {

	public static void main(String[] args) {
		
		Compra compra1 = new Compra();
				
		compra1.cliente = "Antonio";
		
			
		compra1.itens.add(new Item("Caneta", 10, 1.8));
		compra1.itens.add(new Item("Barril", 2, 1500.00));
		compra1.itens.add(new Item("Amendoim", 200, 0.55));
		
		//Bidirecional
		//Ctrl e clique a instancia adicionar...
		compra1.adicionarItem(new Item("Caneta", 10, 1.8));
		compra1.adicionarItem(new Item("Barril", 2, 1500.00));
		compra1.adicionarItem(new Item("Amendoim", 200, 0.55));
		
		// Bidirecional Mais um n�vel
		//Ctrl e clique a instancia adicionar...
		compra1.adicionarItem("Caneta", 10, 1.8);
		compra1.adicionarItem("Barril", 2, 1500.00);
		compra1.adicionarItem("Amendoim", 200, 0.55);
		
		
		System.out.println(compra1.itens.size());
		System.out.println(compra1.obterValorTotal());
		
//		Modelo relacional teste
//       ouble total = compra1.itens.get(0).compra
//				.itens.get(1).compra.obterValorTotal();
//		System.out.println("O total � R$" +total);
		

}
	}
