package OO.composicao.desafio;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
	//Rela��o cliente(1) com (N)compras 
	//final -> Criando uma constante.
	final String nome;
	final List<Compra> compras = new ArrayList<>();
	
	//Instalacia a constante
	Cliente(String nome){
		this.nome = nome;
	}
	
	//Adiciona as compras no array compras
	void adicionarCompra(Compra compra) {
		this.compras.add(compra);
	}
	
	//Pego valor total do Array compras
	double obterValorTotal() {
		double total = 0;
		
		for(Compra compra : compras) {
			total += compra.obterValorTotal();
		}
		return total;
		
		}
	
	
}
