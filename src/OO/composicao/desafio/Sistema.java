package OO.composicao.desafio;


public class Sistema {

	public static void main(String[] args) {
		//Instanciando as classes
		
		Cliente cliente1 = new Cliente("Antonio Jorge");
		Cliente cliente2 = new Cliente("Flavia Penido");
		
		Compra compra1 = new Compra();
		
		compra1.adicionarItem("Caneta", 10, 2);
		compra1.adicionarItem(new Produto("Notebook", 10.22), 2);
		
		Compra compra2 = new Compra();
		compra2.adicionarItem("Pano", 30, 2);
		compra2.adicionarItem(new Produto("Caneta", 100), 2);
		
		//Passado para a classe 
		//cliente1.compras.add(compra1);
		//cliente1.compras.add(compra2);
		
		cliente1.adicionarCompra(compra2);
		cliente1.adicionarCompra(compra1);
		
		System.out.println(cliente1.obterValorTotal());
		System.out.println(cliente2.obterValorTotal());
		
		System.out.printf("O cliente %s gastou um total de %.2f"
				, cliente1.nome, cliente1.obterValorTotal());
		
		
		
		

	}

}
