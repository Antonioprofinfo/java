package OO.composicao.desafio;

import java.util.ArrayList;
import java.util.List;

public class Compra {
	//Reala��o Compra(1) com (N)Items
	final List<Item> itens = new ArrayList<>();
	
	//Instanciando as vari�veis que vem de Item e produto, pois h� uma rela��o Item porduto.
	void adicionarItem(Produto p, int qtd) {
		this.itens.add(new Item(p, qtd));		
	}
	
	void adicionarItem(String nome, double preco, int qtd) {
		//Fazerndo as instancias diretas
		//this.itens.add(new Item(new Produto(nome, preco), qtd));	
		
		//Simplificando
		var produto = new Produto(nome, preco);
		this.itens.add(new Item(produto, qtd));
	}
	
	double obterValorTotal(){
		double total = 0;
		
		//Pego o quantidade de �tens e multiplico por produto
		for(Item item : itens) {
			total += item.quantidade * item.produto.preco;
		}
		
		return total;
	}
}
