package OO.composicao;

public class AlunoCurso {

	public static void main(String[] args) {
		
		Aluno a1 = new Aluno("Antonio");
		Aluno a2 = new Aluno("Flavia");
		Aluno a3 = new Aluno("Gariel");
		
		Curso c1 = new Curso("Informática");
		Curso c2 = new Curso("Contabilidade");
		Curso c3 = new Curso("Matemática");
		
		c1.adicionarAluno(a1);
		c1.adicionarAluno(a2);
		
		c2.adicionarAluno(a1);
		c2.adicionarAluno(a3);
		
		a1.adicionarCurso(c3);
		a2.adicionarCurso(c3);
		a3.adicionarCurso(c3);
		
		
		for (Aluno aluno : c1.alunos) {
			System.out.println(aluno.nome);
		}
		
		for (Aluno aluno : c2.alunos) {
			System.out.println(aluno.nome);
		}
		
		for (Aluno aluno : c3.alunos) {
			System.out.println(aluno.nome);
		}
		
		System.out.println(a1.cursos.get(0).alunos);
		
		Curso cursoEncontrado = a1.obterCursoPorNome("Contabilidade");
		
		if(cursoEncontrado != null) {
			System.out.println(cursoEncontrado.nome);
			System.out.println(cursoEncontrado.alunos);
		}
		
	}

}
