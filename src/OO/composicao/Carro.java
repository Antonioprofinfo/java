package OO.composicao;

public class Carro {
	
	final Motor motor;
	
	Carro(){
		this.motor = new Motor(this);
	}
	
	void acelerar() {
		if(motor.fotorInjecao < 2.6) {
			motor.fotorInjecao += 0.4;
		}
	}
	
	void frear() {
		if(motor.fotorInjecao > 0.5) {
			motor.fotorInjecao -= 0.4;
		}
		
	}
	
	void ligar() {
		motor.ligado = true;
	}
	void desligar() {
		motor.ligado = false;
	}
	
	boolean estaLigado() {
		return motor.ligado;
	}
}
