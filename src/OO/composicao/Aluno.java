package OO.composicao;

import java.util.ArrayList;
import java.util.List;

public class Aluno {
	final String nome;
	final List<Curso> cursos= new ArrayList<>(); // Lista � constante, mas os dados n�o.
	
	Aluno(String nome){
		this.nome = nome;
	}
	
	//Bidirecional
	void adicionarCurso(Curso curso){
		this.cursos.add(curso);
		curso.alunos.add(this);
	}
	
	//Procura o curso e retorna o nome dos alunos
	Curso obterCursoPorNome(String nome) {
		for(Curso curso : this.cursos) {
			if(curso.nome.equalsIgnoreCase(nome));
			return curso;
		}
		return null;
	}
	
	public String toString() {
		return nome;
	}
	
	
}
