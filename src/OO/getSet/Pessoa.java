package OO.getSet;

public class Pessoa {
	private String nome;
	private String sobreNome;
	private int idade;
	
	Pessoa(String nome, String sobreNome, int idade){		
		setNome(nome);
		setSobreNome(sobreNome);
		setIdade(idade);
	}
	
	public int getIdade() {
		return idade;
	}
	public void setIdade(int novaIdade) {
		novaIdade = Math.abs(novaIdade);
		if(novaIdade <= 120) {
			this.idade = novaIdade;
		}else {
			novaIdade = 0;
		}
		
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobreNome() {
		return sobreNome;
	}
	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}
	
	public String getNomeCompleto(){
		return getNome()+" "+getSobreNome();
	}
	
	public String toString(){
		return "Meu nome � " +getNome();
	}
	
	
}
