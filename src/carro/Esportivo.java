package carro;

public interface Esportivo {
	
	//Por padr�o os metodos em uma 
	//interface � public e abstratos
	void ligarTurbo();
	void desligarTurbo();
}
