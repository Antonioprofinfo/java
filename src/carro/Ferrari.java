package carro;

public class Ferrari extends Carro implements Esportivo, Luxo {
	
	private boolean ligarTurbo;
	private boolean ligarAr;
	
	public Ferrari(){
		this(315);
	}
	
	public Ferrari(int velocidadeMaxima){
		super(velocidadeMaxima);
		//delta = 15;
		setDelta(15);
	}
			
	@Override
	public void ligarTurbo() {
		//delta = 35;
		//setDelta(35);
		ligarTurbo = true;
	}		
	
	@Override
	public void desligarTurbo() {
		//delta = 15;
		//setDelta(15);
		ligarTurbo = false;
	}
	
	@Override
	public void ligarAr() {
		ligarAr = true;
	}
	
	@Override
	public void desligarAr() {
		ligarAr = false;
	}
	
	@Override
	public int getDelta() {
		if(ligarTurbo && !ligarAr) {
			return 35;
		}else if(ligarTurbo && ligarAr) {
			return 30;
		} else if(!ligarTurbo && !ligarAr) {
			return 30;
		}else {
			return 15;
		}
	}

}
