package carro;

public class Carro {
	final int VELOCIDADE_MAXIMA;
	int velocidade = 0;
	private int delta = 10;
	
	
	protected Carro(int velocidadeMaxima){
		VELOCIDADE_MAXIMA = velocidadeMaxima;
	}
	
	
	public void acelerar() {
		if (velocidade + getDelta() == VELOCIDADE_MAXIMA) {
			velocidade = VELOCIDADE_MAXIMA;
		}else {
			velocidade += getDelta();
		}
		
	}

	public void frear() {
		if (velocidade >= 5 ) {
			velocidade -= 5;
		}else {
			velocidade = 0;
		}			
		
	}

	public double velocidade() {
		return velocidade;
	}


	public int getDelta() {
		return delta;
	}


	public void setDelta(int delta) {
		this.delta = delta;
	}
	
	
}
