package controle;

public class For1 {

	public static void main(String[] args) {

		/*
		 * int contador = 1; while(contador <= 10) { System.out.println("Bom dia!");
		 * System.out.printf("i = %d\n", contador); contador++; }
		 */
		
		//Utilizado em valor definido.
		for (int contador = 0; contador <= 10; contador++) {
			System.out.printf("i = %d\n", contador);
		}
		
		/* Pouco usual
		int x = 1;
		for (; x <= 10;) {
			System.out.printf("x = %d\n", x);
			x++;
		}
		
		 La�o Infinito
		for(;;) {
			System.out.println("Fim");
		}/*/

	}

}
