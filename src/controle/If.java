package controle;

import java.util.Scanner;

public class If {

	public static void main(String[] args) {
		Scanner entrada = new Scanner (System.in);
		
		System.out.println("Informe a m�dia");
		double media = entrada.nextDouble();
		
		// bloco duplo � obrigat�rio o uso de chaves
		if(media <=10 && media >= 7.0) { 
			System.out.println("Aprovado");
			System.out.println("Parabens!");
		}
		
		//Unico bloco n�o � obrigat�rio o uso de chaves
		if(media>7&& media>=4.5) 
			System.out.println("Recupera��o");
		
		if(media <4.5 && media>0) {
			System.out.println("Reprovado");
		}
		
		//� possivel usar a vari�vel para o c�digo
		boolean criterioAvaliacao = 
				media <4.5 && media>0;
		if(criterioAvaliacao) {
			System.out.println("Reprovado");
		}
		
		
		
		entrada.close();

	}

}
