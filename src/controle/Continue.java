package controle;

public class Continue {

	public static void main(String[] args) {
		
		for (int i = 0; i < 10; i++) {
			if(i%2 == 1) {
				continue; //Interrompe a itera��o(Repeti��o) mas n�o sai do for
			}
			System.out.println(i);
		}
		
		for (int i = 0; i < 10; i++) {
			if(i == 5) continue; //Interrompe a itera��o(Repeti��o)
			System.out.println(i);
		}

	}

}
