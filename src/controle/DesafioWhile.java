package controle;

import java.util.Scanner;

public class DesafioWhile {

	public static void main(String[] args) {
	/*
	 * O aluno vai digitar a nota v�lida
	 * 
	 * Fazer um while que armazene a soma de todas as notas
	 * 
	 * Contar quantas notas valida e inv�lidas foram digitadas
	 * 
	 * Apresentar a m�dia das notas digitadas
	 * 
	 * Para sair digitar -10
	 * 
	 * */
		
		
		 Scanner entrada = new Scanner(System.in);

		double nota = 0;
		int qtdVal = 0;
		@SuppressWarnings("unused")
		int qtdInv = 0;
		double notaValida = 0;
		@SuppressWarnings("unused")
		double notaInvalida = 0;
		double media = 0;
		
		
		while (nota != -10) {
			System.out.print("Diqgite uma nota");
			nota = entrada.nextDouble();
						
			if(nota>10 || nota < 0) {
				System.out.println("Nota inv�lida");
				notaInvalida += nota;
				//System.out.println(notaInvalida);
				qtdInv++;
				//System.out.println(qtdInv);
				
			}else {
				notaValida += nota;
				//System.out.println(notaValida);
				qtdVal++;
				//System.out.println(qtdVal);
			}			
			
		}
		
		media = notaValida /qtdVal;
		System.out.printf("A m�dia final das notas � %.2f", media);

		entrada.close();

	}

}
