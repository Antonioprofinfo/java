package controle;

import java.util.Scanner;

public class DesafioDiaSemana {

	public static void main(String[] args) {
		/*
		 * Domingo -> 1 Quarta -> 2 ....
		 */

		Scanner entrada = new Scanner(System.in);

		System.out.print("Digite o dia da semana ");
		String diaSemana = entrada.nextLine();
		
		/*Posso utilizar das suas formas
		 * equalsIgnoreCase-> Ignora letras maiusculas e mun�sculas 
		 * diaSemana.equalsIgnoreCase("Domingo")
		 * "Domingo".equalsIgnoreCase("diaSemana")
		 * */
		
		
		if (diaSemana.equals("Domingo")) {
			System.out.println("O dia da semana � 1");
		} else if (diaSemana.equals("Segunda")) {
			System.out.println("O dia da semana � 2");
		} else if (diaSemana.equalsIgnoreCase("Ter�a")|| diaSemana.equalsIgnoreCase("Terca")) {
			System.out.println("O dia da semana � 3");
		} else if (diaSemana.equals("Quarta")) {
			System.out.println("O dia da semana � 4");
		} else if (diaSemana.equals("Quinta")) {
			System.out.println("O dia da semana � 5");
		} else if (diaSemana.equals("Sexta")) {
			System.out.println("O dia da semana � 6");
		} else if (diaSemana.equals("S�bado") || diaSemana.equals("Sabado")){
			System.out.println("O dia da semana � 7");
		}else {
			System.out.println("O dia inv�lido");
		}
		
		entrada.close();
		
	}
}
