package controle;

public class ForTres {

	public static void main(String[] args) {

		/* A vari�vel x somente esta dispon�vel no for pois 
		 * foi intanciada no scopo do for.
		for (int x = 0; x < 10; x++) {
			System.out.println(x);
		}
		//N�o � possivel utilizar a variavel x
		System.out.println(x);
		
		
		//Aqui a vari�vel foi criada fora do for
		int i = 0;

		for (; i < 10; i++) {
			System.out.println(i);
		}

		System.out.println("Saiu do for....");
		//Utilizando a vari�vel i
		System.out.println(i);
		*/
		
		for (int i = 0; i <= 10; i++) {
			for(int j = 0; j <=3; j++) {
				System.out.printf("[%d %d]", i, j);
			}
			System.out.println();
		
		}
		
		
		

	}

}
