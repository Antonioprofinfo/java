package controle;

import javax.swing.JOptionPane;

public class IfElse {

	public static void main(String[] args) {
		String valor = JOptionPane.showInputDialog("Infome o n�mero");

		int numero = Integer.parseInt(valor);

		if (numero % 2 == 0) {
			System.out.println("O n�mero � par");
		} else {
			System.out.println("O n�mero � �mpar");
		}
		
		//Pouco usual
		if(numero > 100) {System.out.println("� maior que cem");}else System.out.println("N�o � maior que cem");

	}

}
