package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class DesafioFiltro {

	public static void main(String[] args) {
		Usuario a1 = new Usuario("Antonio", 7.4, "noite");
		Usuario a2 = new Usuario("Gabriel", 9.4,  "noite");
		Usuario a3 = new Usuario("Flavia", 6.4, "manhã");
		Usuario a4 = new Usuario("Valente", 5.4, "tarde");
		
		List<Usuario> aluno = Arrays.asList(a1, a2, a3, a4);
		
		Predicate<Usuario> situacao = n -> n.nota >= 7;
		Predicate<Usuario> turno = t -> t.turno == "noite";
		
		Function<Usuario, String> msg = 
				a -> "O " + a.nome +" do turno da "
				+a.turno+" foi aprovado";
				
		
		aluno.stream()
			.filter(situacao)
			.filter(turno)
			.map(msg)
			.forEach(System.out::println);
	}

}
