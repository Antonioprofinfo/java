package streams;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ImprimindoObjetos {

	public static void main(String[] args) {
		List<String> aprovado = Arrays.asList("Gua", "Fla", "Ant");
		
		System.out.println("1 for");
		for(String nome : aprovado) {
			System.out.println(nome);
		}
		
		System.out.println("2 Interator");
		Iterator<String> i = aprovado.iterator();
		
		while(i.hasNext()) {
			System.out.println(i.next());
		}
		
		System.out.println("3 funcional");
		aprovado.forEach(System.out::println);
		
		System.out.println("4 Stream");
		Stream<String> stream = aprovado.stream();
		stream.forEach(System.out::println);
		
		
	}

}
