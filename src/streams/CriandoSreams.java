package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class CriandoSreams {

	public static void main(String[] args) {
		
		//Consumer -> Interface Funcional que
		//recebe um par�metro e n�o retorna nada;
		Consumer<String> print = System.out::print;
		//Consumer<Integer> println = System.out::println;
		
		//Stream.of cria a stream lista literal;
		Stream<String> langA = Stream.of("Java ", "Php \n");
		langA.forEach(print);
		
		String[] langB = {"Python ", "Lisp ", "Go \n"};
		Stream.of(langB).forEach(print);
		
		//Posso usar o Arrays para montar uma Streams
		Arrays.stream(langB).forEach(print);
		
		//� possivel com array escolher a 
		//posi��o do Arrays
		Arrays.stream(langB, 0,0).forEach(print);
		
		List<String> langC = Arrays.asList("C# ", "C++ ", "Lua \n");
		//Stream.of(langC).forEach(print);  // N�o funciona em list
		langC.stream().forEach(print);
		
		//Stream paralelos
		langC.parallelStream().forEach(print);
		
		//Gerar uma Stream com genetate infinita
		//Stream.generate(()->"a").forEach(print);
		
		//Gerar uma Stream interate infinita
		//Stream.iterate(0, n -> n + 1).forEach(println);
		
	}

}
