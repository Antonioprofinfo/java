package streams;

import java.util.Arrays;
import java.util.List;

public class Outros {

	public static void main(String[] args) {
		Aluno a1 = new Aluno("Ana", 7.1);
		Aluno a2 = new Aluno("Gui", 8.1);
		Aluno a3 = new Aluno("Gab", 6.1);
		Aluno a4 = new Aluno("Ana", 7.1);
		Aluno a5 = new Aluno("Bob", 8.1);
		Aluno a6 = new Aluno("Gab", 9.1);
		
		List<Aluno> alunos = Arrays.asList(a1, a2, a3, a4,a5,a6);
		
		System.out.println("Distinct.......");
		alunos.stream().distinct().forEach(System.out::println);
		
		System.out.println("\nSkip/Limit......");
		alunos.stream()
			.distinct()
			.skip(2)
			.limit(3)
			.forEach(System.out::println);
		
		System.out.println("\ntakeWhile......");
		alunos.stream()
			.distinct()
			.takeWhile(a -> a.nota >= 7)
			.forEach(System.out::println);
		

	}

}
