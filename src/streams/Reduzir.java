package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;

public class Reduzir {

	public static void main(String[] args) {
		
		List<Integer> nums = Arrays.asList(1,2,3,4,5,6,7,8,9);
		
		nums.forEach(System.out::print);
		
		BinaryOperator<Integer> soma = (ac, n) -> ac + n; 
		
		int totalA = nums.stream().reduce(soma).get();
		System.out.println(totalA);
		
		//stream soma uma unica vez
		int totalB = nums.stream().reduce(100,soma);
		System.out.println(totalB);
		
		//stream soma a cada ciclo
		int totalC = nums.parallelStream().reduce(100,soma);
		System.out.println(totalC);
		
		Predicate<Integer> filtro = n -> n > 5;
		
		nums.stream()
			.filter(filtro)
			.reduce(soma)
			.ifPresent(System.out::print);
		
		
	}

}
