package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;

public class Reduzir3 {

	public static void main(String[] args) {

		Aluno a1 = new Aluno("Ana", 7.1);
		Aluno a2 = new Aluno("Gui", 5.1);
		Aluno a3 = new Aluno("Gab", 9.1);
		
		List<Aluno> alunos = Arrays.asList(a1, a2, a3);
		
		alunos.forEach(System.out::println);
		
		Predicate<Aluno> aprovado = a  -> a.nota >= 7;
		Function<Aluno, Double> apenasNota = a -> a.nota;
		
		BiFunction<Media, Double, Media> calcularMedia =
				(media, nota)-> media.adicionar(nota); // Substitui senten�a abaixo
//				{
//					media.adicionar(nota);
//					return media;
//				};
		BinaryOperator<Media> combinarMedia =
				(m1, m2) -> Media.combinar(m1, m2);
				
		Media media = alunos.stream()
			.filter(aprovado)
			.map(apenasNota)
			.reduce(new Media(), calcularMedia, combinarMedia);
			

		System.out.println(media.getValor());

	}

}
