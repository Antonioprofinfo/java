package streams;

import java.util.Arrays;
import java.util.List;
//import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class filtro {

	public static void main(String[] args) {
		
		//Consumer<String> println = System.out::println;
		
		Usuario a1 = new Usuario("Antonio", 7.4, "noite");
		Usuario a2 = new Usuario("Gabriel", 9.4,  "noite");
		Usuario a3 = new Usuario("Flavia", 6.4, "manhã");
		Usuario a4 = new Usuario("Valente", 5.4, "tarde");
		
		List<Usuario> aluno = Arrays.asList(a1, a2, a3, a4);
		
			
		aluno.stream()
			.filter(a -> a.nota >= 7)
			.map(a -> "Parabens " + a.nome + "! vc passou\n")
			.forEach(System.out::print);
		
		Predicate<Usuario> aprovado = a -> a.nota <= 7;
		
		Function<Usuario, String> msg = 
				a -> "Parabens " + a.nome + "! vc não passou\n";
				
		aluno.stream()
		.filter(aprovado)
		.map(msg)
		.forEach(System.out::print);
			

	}

}
