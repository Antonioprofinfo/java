package lambda;

import java.util.function.Function;

public class Desafio {

	public static void main(String[] args) {
		Produto p = new Produto("iPad",3235.89 ,0.50);
		System.out.println(p.nome + "\n"+ p.preco+"\n"+p.desconto);
		
		/*
		 * 1. A partir do produto calcular o preco real (com desconto)
		 * 2. Imposto Municipa >=2500 (8,5%) se <2500 (Isento)
		 * 3. Frete >=3003(100) se <3000 (50)
		 * 4 Arredondar: Deixa 2 casas decimais
		 * 5. Formatar R$1234.56
		 * */
		
		Function<Double, Double> precoReal =
				preco -> p.preco*(1-p.desconto);
				
		System.out.println(precoReal.apply(p.preco));
		
		Function<Double, Double> imposto =
				taxa -> taxa >= 2500 ? 0.085 : taxa;
		
		System.out.println(imposto.apply(precoReal.apply(p.preco)));	
		
		Function<Double, Double> frete =
				precoEnvio -> precoEnvio >= 3000 ? 100.00 : 50.00;
				
		System.out.println(frete.apply(precoReal.apply(p.preco)));
		
		Double preco =p.preco;
		Double PrecoFinal = 
				(precoReal.apply(preco)
						*1+imposto.apply(preco)
						+frete.apply(preco));
		
		System.out.printf("O pre�o final � R$%.2f",PrecoFinal);
	}

}
