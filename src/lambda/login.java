package lambda;

import java.util.function.Function;

public class login {

	public static void main(String[] args) {
		int codCliente = 0;

		Function<Integer, String> usuario = 
				login -> login == 1 ? "Permitido" : "Negado";
		
		Function<String, String> logado =
				dados -> dados == "Permitido"
				? dados + " -> admin/index.htl" 
				: dados + " -> user/index.html";
				
		Function<String, String> redurecao =
				dados -> dados + " -> sucesso!";
		
		String acesso = usuario
				.andThen(logado)
				.andThen(redurecao)
				.apply(codCliente);
		
		System.out.println(acesso);
	}

}
