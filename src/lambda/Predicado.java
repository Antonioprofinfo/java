package lambda;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Predicado {

	public static void main(String[] args) {
		Produto produto = new Produto("Sofá", 10.22, 0.02);
		
		//System.out.println(produto.preco);
		
		Predicate<Produto> isCaro = prod->prod.preco <=700;
				
		Consumer<Produto> cons3 = nome->System.out.println("Teste3");
		cons3.accept(produto);		
		
		Consumer<Produto> cons = nome->System.out.println(produto.nome); 
		Consumer<Produto> cons1 = nome->System.out.println(produto.preco);
		Consumer<Produto> cons2 = nome->System.out.println(produto.desconto);
		
		
		cons.accept(produto);
		cons1.accept(produto);
		cons2.accept(produto);
		
		
		System.out.println(isCaro.test(produto));
		
		if(isCaro.test(produto)){
			System.out.println("Verdadeiro");
			
		Function<Integer, String> parOuImpar 
		= numero -> numero % 2 == 0 ? "Par" : "Imapar";
		
		System.out.println(parOuImpar.apply(10));
		
		Function<String, String> resultado 
		= valor -> "O resultado é " + valor;
		
		Function<String, String> empolgado =
				valor -> valor + "!!!";
		
		String resultadoFinal= parOuImpar
				.andThen(resultado)
				.andThen(empolgado)
				.apply(30);
		
		
		
		System.out.println(resultadoFinal);
		
		
		}
		

	}

}
