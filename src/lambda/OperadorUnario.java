package lambda;

import java.util.function.UnaryOperator;

public class OperadorUnario {

	public static void main(String[] args) {
		UnaryOperator<Integer> maisDois = n-> n+2;
		UnaryOperator<Integer> vezesDois = n-> n*2;
		UnaryOperator<Integer> aoQuadrado = n-> n * n;
		
		
		//anthen -> De cima para baixo
		int resultado = maisDois
				.andThen(vezesDois)
				.andThen(aoQuadrado)
				.apply(1);
		
		
		System.out.println(resultado);
		
		//compose -> De baixo para cima
		int resultado2 = maisDois
				.compose(vezesDois)
				.compose(aoQuadrado)
				.apply(2);
		
		
		System.out.println(resultado2);

	}

}
