package lambda; 

import java.util.function.Function;

public class Funcao {

	public static void main(String[] args) {
		
 		Function<Integer, String> parOuImpar =
 				numero -> numero % 2 == 0 ? "Par" : "Impar";
 				
 		System.out.println(parOuImpar.apply(30));
 		
 		Function<String, String> resultado =
 				valor -> "O número é " + valor;
 				
 		Function<String, String> empolgado =
 				valor -> valor + "!!!"
 				;
 				
 		String resultadoFinal = parOuImpar
 				.andThen(resultado)
 				.andThen(empolgado)
 				.apply(30);
 		
 		System.out.println(resultadoFinal);

	}

}
