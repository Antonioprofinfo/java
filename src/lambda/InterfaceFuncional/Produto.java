package lambda.InterfaceFuncional;
//A classe M�e de todas as classes � Object
// public class Cliente extends Object //Ficando implicito
public class Produto {
	
	final String nome;
	final double preco;
	final double desconto;
	
	
	public Produto(String nome, double preco, double desconto) {
		this.nome = nome;
		this.preco = preco;
		this.desconto = desconto;
	}
	
	
}
