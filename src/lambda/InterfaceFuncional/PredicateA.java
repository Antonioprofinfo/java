package lambda.InterfaceFuncional;

import java.util.function.Predicate;

public class PredicateA {
//https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html
	public static void main(String[] args) {
		//Retorna saida falso
		//Predicate<Cliente> isCaro = prod -> false;
		
		//Segunda mudan�a
		//Predicate<Cliente> isCaro = prod -> prod.preco >= 750;
		
		Predicate<Produto> isCaro = prod -> (prod.preco*(1-prod.desconto)) >= 750;
		
		Produto produto = new Produto("Cadeira",3893.89, 0.15);
		System.out.println(isCaro.test(produto));
	}

}
