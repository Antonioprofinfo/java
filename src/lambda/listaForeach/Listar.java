package lambda.listaForeach;

import java.util.Arrays;
import java.util.List;


public class Listar {

	public static void main(String[] args) {
		List<String> usuario = Arrays.asList("Gui", "Fla");
		
		System.out.println("Forma tradicional...");
		for(String nome : usuario) {
			System.out.println(nome);
		}
		
		System.out.println("\n Forma de Lambda 01");
		usuario.forEach(nome->System.out.println(nome));
		usuario.forEach(nome->imprimir());
		
		System.out.println("\n Metodo de referencia 01");
		usuario.forEach(System.out::println);
		//usuario.forEach(Foreach::imprimir);
		
	}
	
	static void imprimir() {
		System.out.println("Imprimindo ....");
	}

}
