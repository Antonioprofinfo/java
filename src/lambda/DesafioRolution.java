package lambda;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class DesafioRolution {

	public static void main(String[] args) {
			
			
			/*
			 * 1. A partir do produto calcular o preco real (com desconto)
			 * 2. Imposto Municipa >=2500 (8,5%) se <2500 (Isento)
			 * 3. Frete >=3003(100) se <3000 (50)
			 * 4 Arredondar: Deixa 2 casas decimais
			 * 5. Formatar R$1234.56
			 * */
		Function<Produto, Double> precoReal =
				p -> p.preco * (1-p.desconto);
							
		UnaryOperator<Double> imposto =
				preco -> preco >= 2500 ? preco * 1.085 : preco;
			
		UnaryOperator<Double> frete =
				preco -> preco >= 3000 ? preco + 100 : preco + 50;
				
		UnaryOperator<Double> arredondar =
				preco ->Double.parseDouble(String.format("%,2f", preco));
		
		Function<Double, String> formatar =
						preco -> ("R$"+preco).replace(".",",");	
						
		Produto p = new Produto("iPad",3235.89 ,0.13);	
		String preco = precoReal
				.andThen(imposto)
				.andThen(frete)
				.andThen(arredondar)
				.andThen(formatar)
				.apply(p);
		
		System.out.println("O pre�o final � "+ preco);

	}

}
