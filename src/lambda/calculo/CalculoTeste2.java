//package lambda.calculo;
//
//public class CalculoTeste2 {
//
//	public static void main(String[] args) {
//		
//		
//		
//		//fun��o an�nima 
//		//Aa chaves obrigam o retorno
//		Calculo somar = (x, y)->{
//			return x + y;
//		};
//		System.out.println(somar.executar(2, 6));
//		
//		
//		//Sem chaves, n�o colocar o return
//		//Ter� apenas uma sentensa
//		somar =(x, y)->x*y;
//		System.out.println(somar.executar(2, 3));
//		
//		Calculo multiplicar = (x, y)->{
//			return x * y;
//		};
//		System.out.println(multiplicar.executar(2, 3));
//		
//		Calculo potencia = (x, y)->{
//			return Math.pow(x, y);
//		};
//		System.out.println(potencia.executar(2, 3));
//		
//		System.out.println(somar.legal());
//		
//		//Posso instaciar a interface.
//		System.out.println(Calculo.muitoLegal());  
//		
//		
//	}
//
//}
