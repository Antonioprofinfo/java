package lambda.calculo;

import java.util.Arrays;
import java.util.List;

public class CalcularTeste {
	public static void main(String[] args) {
	Calculo calc = (x, y)->{
		return 2 * 3;
	};
	
	System.out.println(calc.operacao(2, 5));
	
		
//	
//	List<String> aprovado = Arrays.asList("Ana", "Bia", "Gui", "Lic");
//	
//	for(String nome : aprovado) {
//		System.out.println(nome);
//	}
//	
//	System.out.println("\n Foreach Lambda 1");	
//	aprovado.forEach(nome->System.out.println(nome +"!"));
//	
//	System.out.println("\n Method References");
//	aprovado.forEach(System.out::println);
	
	List<String> aprovado = Arrays
			.asList("Fla","Flu","Vas","Bot");
	
	for(String nome : aprovado) {
		System.out.println(nome);
	}
	
	System.out.println("\n Method Lambda 01 e 02");
	aprovado.forEach(nome->System.out.println(nome+"!"));
	aprovado.forEach(nome->imprimir(nome));
	
	System.out.println("\n Method References 01 e 02");
	aprovado.forEach(System.out::println);
	//aprovado.forEach(foreach::imprimir);
}
		
	
	static void imprimir(String nome) {
		System.out.println(nome);
	}
}

// Bradesco Ag0292.5
// cc  92.830.5
// 10.00
