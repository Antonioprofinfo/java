package lambda.calculo;

import java.util.function.BinaryOperator;

public class CalculoTeste3 {

	public static void main(String[] args) {
		//fun��o an�nima 
		//Aa chaves obrigam o retorno
		//erro no apply de convers�o
		//double -> Double � poss�vel
		//int -> Double n�o permite
		BinaryOperator<Double> calc = (x, y)->{
			return x + y;
		};
		System.out.println(calc.apply(2.0, 6.0));
		
		BinaryOperator<Integer> calc1 = (x, y)->{
			return x + y;
		};
		System.out.println(calc1.apply(2, 6));
		
		//Sem chaves, n�o colocar o return
		//Ter� apenas uma sentensa
		calc =(x, y)->x*y;
		System.out.println(calc.apply(2.0, 3.0));
		
		
	}

}
