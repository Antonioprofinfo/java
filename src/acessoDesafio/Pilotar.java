package acessoDesafio;

import carro.Ferrari;
import carro.Fusca;

public class Pilotar {

	public static void main(String[] args) {
		Ferrari ferrari = new Ferrari();
		ferrari.ligarTurbo();
		System.out.println(ferrari.velocidadeAr());
		ferrari.ligarAr();
        ferrari.acelerar();
        ferrari.acelerar();
        ferrari.desligarTurbo();
 		ferrari.acelerar();
 		
 		
		ferrari.frear();
		
		System.out.printf("Velocidade da ferrari %.2f \n", 
				ferrari.velocidade());
		
		
		Fusca fusca = new Fusca();
		
		fusca.acelerar();
		//fusca.acelerar();
		//fusca.acelerar();
		
		//fusca.frear();

		System.out.printf("Velocidade da fusca %.2f \n", 
				fusca.velocidade());

	}

}
