package Colecoes;

import java.util.ArrayList;
import java.util.List;

public class Lista {

	public static void main(String[] args) {
		
		//ArrayList<Usuario> lista = new ArrayList<>();
		List<Usuario> lista = new ArrayList<>(); // Pode ser assim tamb�m
		
		Usuario u1 = new Usuario("Ana");
		lista.add(u1);
		
		//System.out.println(u1.nome);
		lista.add(new Usuario("Beto"));
		lista.add(new Usuario("Carlos"));
		lista.add(new Usuario("Bia"));
		
		//System.out.println(lista.get(3).nome);
		//Observe o Metodo toString
		System.out.println(lista.get(3)); //Acesso pelo �ndice
		
		lista.remove(1);
		System.out.println(lista.remove(new Usuario("Carlos")));
		System.out.println(lista.contains(new Usuario("Carlos")));
		System.out.println(lista.contains(new Usuario("Bia")));
		
		
		for (Usuario usuario : lista) {
			System.out.println(usuario.nome);
			//System.out.println(usuario);
		}
		
	}

}
