package Colecoes;

import java.util.LinkedList;
import java.util.Queue;

public class Fila {

	public static void main(String[] args) {
		
		Queue<String> fila = new LinkedList<>();
		
		//Adicionam elementos da fila
		//Add d� erro em fila pre-definida
		//Offer n�o dar� erro apenas retorn false.
		fila.add("Antonio"); //Retorna falso
		fila.offer("Gabriel"); //// Erro na execu��o
		fila.add("Carlos");
		fila.offer("Flavia");
		
		//Retornar fila
		System.out.println(fila.peek()); //Retorna null
		System.out.println(fila.element()); // Erro na execu��o
		
		fila.size(); //Tamamho da fila
		//fila.clear(); //Limpa a fila
		fila.isEmpty(); 
		//System.out.println(fila.contains(fila));
		
		//Verifica de a lista est� vazia
		
		//Remove a lista
		System.out.println(fila.size());
		System.out.println(fila.poll());
		System.out.println(fila.poll());
		System.out.println(fila.poll());
		System.out.println(fila.poll());		
		System.out.println(fila.poll());//Retorna null
		//System.out.println(fila.remove()); // Se a lista estiver vazia d� Erro na execu��o
	
		System.out.println(fila);
		
	}

}
