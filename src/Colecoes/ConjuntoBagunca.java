package Colecoes;

import java.util.HashSet;
import java.util.Set;

public class ConjuntoBagunca {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		
		HashSet conjunto = new HashSet();
		
		conjunto.add(1.2);
		conjunto.add(true);
		conjunto.add("Teste");
		conjunto.add(1);
		conjunto.add("x");
		
		System.out.println("Tamanho � " + conjunto.size());
		System.out.println(conjunto);
			
		conjunto.add("teste");
		System.out.println("Tamanho � " + conjunto.size());
				
		System.out.println(conjunto.remove("Teste"));
		System.out.println("Tamanho � " + conjunto.size());
		
		System.out.println(conjunto.contains("Teste"));
		System.out.println(conjunto.contains("x"));
		
		Set nums = new HashSet();
		nums.add(1);
		nums.add(2);
		nums.add(3);
		System.out.println(nums);
		
		conjunto.addAll(nums); //Uni�o entre dois conjuntos
		System.out.println(conjunto);
		
		conjunto.retainAll(nums); //Valor incomun entre os conjuntos
		System.out.println(conjunto);
		
		conjunto.clear();
		System.out.println(conjunto);
		
		
	}

}
