package Colecoes;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Mapa {

	public static void main(String[] args) {
		
		//Estrutura chave valor		
		//Map usuario = new Map();
		Map<Integer, String> usuario = new HashMap<>();
		
		usuario.put(1, "Antonio"); //Adiciona ou substitui caso exista
		usuario.put(2, "Gabriel");
		usuario.put(3, "Flavia");
		
		System.out.println(usuario);
		System.out.println(usuario.size());
		System.out.println(usuario.isEmpty());
		
		System.out.println(usuario.keySet());
		System.out.println(usuario.values());
		System.out.println(usuario.entrySet());
		System.out.println(usuario.containsKey(20));
		System.out.println(usuario.containsValue("Antonio"));
		System.out.println(usuario.get(2));
		System.out.println(usuario.remove(2));
		System.out.println(usuario.remove(3, "Gui"));
		
				
		for(int u : usuario.keySet()) {
			System.out.println(u);
		}
		
		for (String n : usuario.values()) {
			System.out.println(n);
		}
		
		for (Entry<Integer, String> registro : usuario.entrySet()) {
			System.out.println(registro);
			
			System.out.println(registro.getKey());
			System.out.println(registro.getValue());
		}
	}

}
