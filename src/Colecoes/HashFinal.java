package Colecoes;

import java.util.HashSet;

public class HashFinal {

	public static void main(String[] args) {
		
		HashSet<Usuario> usuarios = new HashSet<>();
		
		usuarios.add(new Usuario("Antonio"));
		usuarios.add(new Usuario("Gabriel"));
		usuarios.add(new Usuario("Flavia"));
		
		System.out.println(usuarios.contains(new Usuario("Gabriel")));
		
		

	}

}
