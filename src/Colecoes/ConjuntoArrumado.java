package Colecoes;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@SuppressWarnings("unused")
public class ConjuntoArrumado {

	public static void main(String[] args) {
		// Set<String> lista = new HashSet<> (); // N�o ordenado
		HashSet<String> lista = new HashSet<>(); // N�o ordenado

		lista.add("Antonio");
		lista.add("Flavia");
		lista.add("Gabriel");
		lista.add("Bernardo");
		lista.add("Solange");

		System.out.println(lista);

		for (String alunos : lista) {
			System.out.println(alunos);
		}

		Set<Integer> numns = new HashSet<>(); // N�o ordenado

		numns .add(1);
		numns .add(2);
		numns .add(3);
		numns .add(4);
		numns .add(5);
		
		//numns.get(3); N�o � poss�vel pegar o �ndice

		System.out.println(numns );

		for (int n : numns ) {
			System.out.println(n);
		}

		// SortedSet<String> listaAprovados = new TreeSet<>(); Ordenado
		TreeSet<String> listaAprovados = new TreeSet<>();// Ordenado

		listaAprovados.add("Antonio");
		listaAprovados.add("Flavia");
		listaAprovados.add("Gabriel");
		listaAprovados.add("Bernardo");
		listaAprovados.add("Solange");

		System.out.println(listaAprovados);

		for (String alunos : listaAprovados) {
			System.out.println(alunos);
		}
	}

}
