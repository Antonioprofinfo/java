package fundamentos;

import java.util.Scanner;

public class Console {

	public static void main(String[] args) {
		System.out.print("Bom");
		System.out.print(" dia");
		
		System.out.println("Bom");
		System.out.println(" dia");
		
		System.out.printf("Idades: %d %d %n", 41, 42);
		System.out.printf("sal�rio: %.2f%n", 1235.3587);
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Digite seu nome: ");
		String nome = entrada.nextLine();
		
		System.out.print("Digite seu sobrenome: ");
		String sobrenome = entrada.nextLine();
		
		System.out.print("Digite sua idade: ");
		int idade = entrada.nextInt();
		
		System.out.printf("\n%s \n%s \n%d", nome, sobrenome, idade);
		
		entrada.close();
	}

}
