package fundamentos;

public class Inferencia {

	public static void main(String[] args) {
		double a = 4.5; //vari�vel foi declarada e inicializada
		System.out.println(a);
		
		var b = 4.5; //Inferencia
		System.out.println(b);
		
		var c = "Texto";
		System.out.println(c);
		
		// var c = 4.5; n�o � permitido mudar o tipo de v�ri�vel. 
		//O java � fortemente tipado.
		//System.out.println(c);
		
		double d; //vari�vel foi declarada
		d =125.12; //Variavel inicializada
		System.out.println(d); //Vari�vel usada
		
		//N�o � possivel
		//var e;
		// e = 125,12
		
		// var f 12;  //inteiro
		//f = 12.01; //double
		//Vai dar erro.
	}

}
