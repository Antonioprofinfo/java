package fundamentos;

import java.util.Scanner;

public class DesafioCalculadora {

	public static void main(String[] args) {
		// Ler num 1.
		// Ler num 2.
		// Pedir os calculos.
		// Mostra resultados.
		
		Scanner entrada = new Scanner(System.in);
		System.out.println("Digite um operador + - * / %");
		String operador = entrada.next();
		
		System.out.println("Digite o valor 1");
		double num1 = entrada.nextDouble();
		
		System.out.println("Digite o valor 2");
		double num2 = entrada.nextDouble();
		
				
		/*boolean soma = ("+".equals(operador));
		boolean subtracao = ("-".equals(operador));
		boolean multiplicacao = ("*".equals(operador));
		boolean divisao = ("/".equals(operador));
		boolean mod = ("%".equals(operador));
			
		
		double opSoma = num1 + num2;
		double opSubtracao = num1 - num2;
		double opMultiplicacao = num1 * num2;
		double opDivisao = num1 / num2;
		double opMod = num1 % num2;
		
		double resultado = (double) (
				soma ? opSoma :
				subtracao? opSubtracao:
				multiplicacao ? opMultiplicacao :
				divisao? opDivisao :
				opMod);
	    */
		
		double resultado = "+".equals(operador)?num1+num2 :0;
		resultado = "-".equals(operador)?num1-num2 :resultado;
		resultado = "*".equals(operador)?num1*num2 :resultado;
		resultado = "/".equals(operador)?num1/num2 :resultado;
		resultado = "%".equals(operador)?num1%num2 :resultado;
		
		System.out.printf("%f %s %f = %.2f", num1, operador, num2, resultado);
	   
	   
		entrada.close();
		
		
		
		

	}

}
