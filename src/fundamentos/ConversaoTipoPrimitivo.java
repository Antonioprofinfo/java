package fundamentos;

public class ConversaoTipoPrimitivo {

	public static void main(String[] args) {
		double a = 1; // Implicita
		
		System.out.println(a);
		
		//float b = 1.0F;
		//Explicita (CAST)
		float b = (float)1.12545;
		System.out.println(b); 
		
		int c = 4;
		byte d = (byte)c;
		System.out.println(d); 
		
		
	}

}
