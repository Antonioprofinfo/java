package fundamentos;

public class TiposPrimitivos {

	public static void main(String[] args) {
		//Informa��o do Funcion�rio
		
		//Tipos Num�ricos Inteiros
		byte anosDeEmpresa = 23;
		short numeroDeVoos = 542;
		int id = 56789;
		long pontosAcumulados = 3_234_845_22L;//O "L" define um literal Long caso eu passe do tamanho do int
		
		//Tipos num�rico reais
		float salario = 11_445.44F; ////O "F" define um literal float para identifica n�meros acima de double
		double vendasAcumulas = 2_991_797_103.01;	
		
		//Tipos boolean
		boolean estaDeFerias = false; //true
		
		//Tipo caracter
			char status = 'A'; //Ativo
			
		System.out.println(anosDeEmpresa +"\n" 
		+ numeroDeVoos +"\n"
		+ salario +"\n"
		+ vendasAcumulas +"\n"
		+ id +"\n" 
		+ pontosAcumulados +"\n" 
		+ estaDeFerias +"\n" 
		+ status);	
			

	}

}
