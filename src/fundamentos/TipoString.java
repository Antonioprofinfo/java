package fundamentos;

public class TipoString {

	public static void main(String[] args) {
		System.out.println("Ol� pessoal".charAt(4));
		
		String s = "Boa tarde!";
		s = s.toUpperCase();
		System.out.println(s.concat("!"));
		
		s = "Bom dia".concat("!" + "!!");
		System.out.println(s);
		
		System.out.println(s.startsWith("Boa"));
		System.out.println(s.startsWith("Bom"));
		
		System.out.println(s.startsWith("bom"));
		System.out.println(s.endsWith("dia!!!"));
		
		System.out.println(s.toLowerCase().startsWith("bom"));
		System.out.println(s.toUpperCase().startsWith("BOM"));
		
		System.out.println(s.length());
		
		System.out.println(s.equals("bom dia!!!"));
		System.out.println(s.equalsIgnoreCase("bom dia!!!"));
		
		var nome = "Antonio";
		var sobrenome = "Jorge";
		var idade = 41;
		var salario = 1200.25;
		
		String maisFrases = "Nome: "+nome
				+" \nSobrenome: " +sobrenome
				+ "\nIdade: "+idade
				+" \nSal�rio: " + salario+ "\n";
				
		System.out.println(maisFrases);
		
		System.out
		.println("Nome: "+nome
				+" \nSobrenome: " +sobrenome
				+ "\nIdade: "+idade
				+" \nSal�rio: " + salario + "\n");
		
		System.out.printf("Nome: %s \nSobrenome:%s \nIdade: %d \nSal�rio: R$%.2f \n\n" 
				,nome, sobrenome, idade, salario );
		
		var frase = String.format("Nome: %s \nSobrenome:%s \nIdade: %d \nSal�rio: R$%.2f"
				,nome, sobrenome, idade, salario);
		
		System.out.println(frase.indexOf("Sobre"));
		System.out.println(frase.substring(15,20)); //Inicio e fim do texto
		
	}

}
