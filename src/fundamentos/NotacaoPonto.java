package fundamentos;

public class NotacaoPonto {

	public static void main(String[] args) {
	
		String s = "Bom dia X";
		System.out.println(s);
		
		String t = "Bom dia X";
		t.toUpperCase(); //N�o � propagado
		System.out.println(t);
		
		t = "Bom dia X";
		t = t.toUpperCase(); //propagado
		System.out.println(t);
		
		t = t.replace("X", "Senhora");
		t = t.concat("!!!!");
		System.out.println(t);
		
		System.out.println("Antonio".toUpperCase());
		
		//No ponto � o melhor local para subdividir o c�digo. 
			//Observe que a syntax somente termina no ;
		String y = "Boa noite X"
				.replace("X", "Flavia")
				.toUpperCase()
				.concat("!!!");
		
		System.out.println(y);
		
		//Tipos primotivos n�o tem o operador "."
		
	}

}
