package fundamentos;

import java.util.Scanner;

public class DesafioConversao {

	public static void main(String[] args) {
		// A partir do Scanner
		//Pegar 3 String com NextLine
		//String com Sal�rio de Um funcion�rio
		//Calcule a m�dia dos 3 sal�rios
		//O funcion�rio pode utilizar virgula 
		//ou ponto ao digitar
		
		
		Scanner Entrada = new Scanner(System.in);
		
		System.out.println("Digite o primeiro Salário");
		String salario1 = Entrada.nextLine().replace(",", ".");
		//salario1 = salario1.replace((","),("."));
		
		System.out.println("Digite o Segundo Salario");
		String salario2 = Entrada.nextLine().replace(",", ".");
		//salario2 = salario2.replace((","),("."));
		
		System.out.println("Digite o Terceiro Sal�rio");
		String salario3 = Entrada.nextLine().replace(",", ".");
		//salario3 = salario3.replace((","),("."));
		
		System.out.printf("%s %s %s", salario1, salario2, salario3);
		double sal1 = Double.parseDouble(salario1);
		double sal2 = Double.parseDouble(salario2);
		double sal3 = Double.parseDouble(salario3);
		
		System.out.printf("\n %.2f %.2f %.2f \n", sal1, sal2, sal3);
		
		double media = (sal1+sal2+sal3)/3;
		System.out.printf("%.2f",media);
		
		Entrada.close();

	}

}
