package fundamentos;

public class ConversaoNumeroString {

	public static void main(String[] args) {
		
		//Wrapper
		Integer num1 = 10000;
		
		System.out.println(num1.toString().length());
		
		//Primitivo
		int num2 = 100000;
		System.out.println(Integer.toString(num2).length());
		
		double num3 = 100.321;
		System.out.println(Integer.toString(num2).length());
		
		//Converte numero em texto
		System.out.println(("" + num1).length());
		System.out.println(("" + num2).length());
		System.out.println(("" + num3).length());
		
		

	}

}
