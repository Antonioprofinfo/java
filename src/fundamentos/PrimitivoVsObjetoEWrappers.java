package fundamentos;

public class PrimitivoVsObjetoEWrappers {

	public static void main(String[] args) {
		//Wrappers s�o a vers�o obetos dos tipos primitivos
		
		//byte, short, int, long, float, double, boolean, char -> Primitivo
		//Byte, Short, Integer, Long, Float, Double Boolean, Character-> Objetos
		
		Byte b = 100;
		Short s	= 1000;
		Integer i = 10000;
		//Long l = 100000L;
		
		Float f = 123.45F;
		Double d = 1235.4567;
		
		Boolean bo = true;
		
		Character c = '#'; 
		
		//Transformar Valores em String
		
		System.out.println(b.toString());
		System.out.println(s.toString());
		System.out.println(i.toString());
		System.out.println(f.toString());
		System.out.println(d.toString());
		System.out.println(bo.toString());
		System.out.println(c.toString());
			
		/*
		String vb = "100", vs="1000", vi="100000", vl="100000", vf="123.45", vd="1235.4575", vbo="false", vc="A";
		
		
		
		Byte bb = Byte.parseByte(vb);
		Short ss = Short.parseShort(vs);
		Integer ii = Integer.parseInt(vi);
		Long ll = Long.parseLong(vl);
		
		Float ff = Float.parseFloat(vf);
		Double dd = Double.parseDouble(vd);
		
		Boolean boo = Boolean.parseBoolean(vbo);
		
		//N�o � poss�vel converter String para Character.
		//Character cc = Character.parseCharacter(vc); 
		
		
			
		System.out.println(bb.TYPE);
		System.out.println(ss.TYPE);
		System.out.println(ii.TYPE);
		System.out.println(ll.TYPE);
		
		System.out.println(ff.TYPE);
		System.out.println(dd.TYPE);
		
		System.out.println(boo.TYPE);*/
		
		
		
		
		
	}

}
