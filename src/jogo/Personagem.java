package jogo;

public class Personagem {
	public int vida = 100;
	
	public int posX;
	public int posY;
	
	 public Personagem(int posX, int posY){
		this.posX = posX;
		this.posY = posY;
		
	}
	
	
	boolean andar(Movimento movimento) {
		if(movimento == Movimento.CIMA) {
			posX++;
		}else if(movimento == Movimento.BAIXO){
			posX--;			
		}else if(movimento == Movimento.ESQUERDA){
			posY++;			
		}else if(movimento == Movimento.ESQUERDA){
			posY--;			
		}
	return true;
	}
	
		
	
	public boolean atacar(Personagem oponente) {
			int deltaX = Math.abs(posX - oponente.posX);
			int deltaY = Math.abs(posY - oponente.posY);
			
			if(deltaX == 0 && deltaY == 1) {
				oponente.vida -= 30;			
				return true;
			} else if(deltaX == 1 && deltaY == 0) {
				oponente.vida -= 30;	
				return true;
			}
			return false;
			}
	
	public String venceu() {
		return "Você venceu";
		
	}
	
	
	public String jogando() {
		return "Jogo rolando";
		
	}
	
	public String derrota() {
		return "Você Perdel";
		
	}	
		
}
