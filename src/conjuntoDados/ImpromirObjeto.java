package conjuntoDados;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ImpromirObjeto {
	//Tipos de Stream
	//Built->Int->int->Terminal
	//Ordenada, n�o ordenda
	// Strem n�o paralelo e Stream Paralelo
	

	public static void main(String[] args) {
		List<String> aprovado = Arrays.asList("Antonio","Flavia","Gabriel");
		
		
		System.out.println("Usando o Foreach..");
		for(String nome : aprovado) {
			System.out.println(nome);
		}
		
		System.out.println("\nUsando o Iterator...");
		Iterator<String> iterator = aprovado.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		
		System.out.println("\nUsando o Stream...");
		
		Stream<String> stream = aprovado.stream();
		stream.forEach(System.out::println);
		
			
		}

	}


