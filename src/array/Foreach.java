package array;


public class Foreach {

	public static void main(String[] args) {
		
		double[] notas = {9, 8, 7, 4, 5};
		
		//Comum
		for (int i = 0; i < notas.length; i++) {
			System.out.print(notas[i] );
		}
		System.out.println();
		
		//Foreach
		for (double nota : notas) {
			System.out.print(nota);
		}

	}

}
