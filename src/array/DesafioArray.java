package array;

import java.util.Arrays;
import java.util.Scanner;

public class DesafioArray {

	public static void main(String[] args) {
		/* Informar quantas notas
		 * Ap�s criar array com a quantidade
		 * 2 for
		 * 	 1� Pegar as notas e armazenar em um array
		 *   2� usando o foreach, voc� somar� as notas
		 *      e apresenta a m�dia;
		 * */
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Digite a quantidade notas");
		int quantidade = entrada.nextInt();
		
		int[] nota = new int[quantidade];
		
		System.out.println(Arrays.toString(nota));
		
		int posicao = 1;
		//double nota = 0;
		double soma = 0;
		for (int i = 0; i < nota.length; i++) {
			System.out.println("Digite a nota "+posicao);
			nota[i] = entrada.nextInt();
			posicao++;
		}
		
		for (int i : nota) {
			soma += i;			
		}
		
		double media = soma / nota.length;
		System.out.println(Arrays.toString(nota));
		System.out.println(soma);
		System.out.println(media);
		
		entrada.close();

	}

}
