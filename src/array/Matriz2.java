package array;

import java.util.Arrays;
import java.util.Scanner;

public class Matriz2 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Digite a quantidade de Aluno");
		int alunos = entrada.nextInt();
		
		System.out.println("Digite a quantidade de notas");
		int notas = entrada.nextInt();
		
		double[][] notasDaTurma = new double[alunos][notas];
		
		double valorTotal = 0;
		double mediaTotal = 0;
		int contador = 0;
								
		for(int a = 0; a < notasDaTurma.length; a++) {			
			for(int n = 0 ; n < notasDaTurma[a].length; n++) {
				System.out.printf("Informe as notas %d no aluno %d: ",
						n + 1, a + 1);
				notasDaTurma[a][n] = entrada.nextDouble();
				valorTotal += notasDaTurma[a][n];
				contador++;
				
			}
					
		}
		
		//System.out.println(Arrays.toString(totalNotas));
		System.out.println(valorTotal);
		
		mediaTotal = valorTotal / contador ;
		System.out.println(mediaTotal);
		
		mediaTotal = valorTotal / (alunos * notas);
		System.out.println(mediaTotal);
		
		//foreach
		for (double[] notasdosAluno : notasDaTurma) {
			System.out.println(Arrays.toString(notasdosAluno));
		}
				
		
		entrada.close();

	}

}
