package array;

import java.util.Arrays;

public class exercicio {

	public static void main(String[] args) {
		
		double[] a = new double[3];
		System.out.println(Arrays.toString(a));
		
		a[0]=10;
		a[1]=9;
		a[2]=8;
		
		System.out.println(a.length);
		
		System.out.println(Arrays.toString(a));
		System.out.println(a.length);
		
		double total =0 ;
		for(int x = 0; x < a.length; x++) {
			total += a[x];
		}
		
		
		//foreach
		for (double d : a) {
			System.out.print(d + " ");
		}
		
		System.out.println(total);
		System.out.println(total / a.length);
		System.out.println(total / a.length);
		
		// Inserir dados diretamente no array
		
		double[] b = { 9, 2, 3 ,4};
		
		//double total = 0;
		for(int i = 0; i < b.length; i++) {
			System.out.println(b[i]+ " ");
		}
		
		//foreach
		for (double d : b) {
			System.out.println(d + " ");
		}
		
		//var x = {1,20};
		double[] c = {1,2};
		System.out.println(c.length);
		
		
		
		
		

	}

}
