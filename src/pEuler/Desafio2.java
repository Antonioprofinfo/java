package pEuler;
/*Each new term in the Fibonacci sequence is generated 
 * by adding the previous two terms. By starting with 1 and 2,
 *  the first 10 terms will be:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

By considering the terms in the Fibonacci sequence 
whose values do not exceed four million, find the sum 
of the even-valued terms.

F(n + 2) = F(n + 1) + F(n) , com n ≥ 1 e F(1) = F(2) = 1

Resultado (33)3524578
somar par4613732
 * */

public class Desafio2 {

	public static void main(String[] args) {
		
		int y = 0;
		int z = 1;
		int w;
	    long f1 = 0 ;
	    long f2 = 0;
					
			for(int x = 2; x<34;x++){
				w=y+z;
				
				System.out.println("("+x+")"+w);
				
				y=z;
				z=w;
				
				if(w%2 == 0) {
					f1+=w;
					System.out.println("somar par"+f1);
				}else {
					f2+=w;
					System.out.println("somar ímpar"+f2);
				}		
			
			
		}
		
			System.out.println("Soma Par \n "+f1);
			System.out.println("Soma ímpar \n "+f2);
		
		

		
	}

}
