package pEuler;
/*If we list all the natural numbers below 10 that 
 * are multiples of 3 or 5, we get 3, 5, 6 and 9.
 *  The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
 * */
public class Desafio1 {

	public static void main(String[] args) {
		int z = 3;
		double y = z%3;
		System.out.println(y);
		int total = 0;
		
		for(int x = 1; x < 1000; x++) {
			if(x%3 == 0) {
				total += x;
			}else if(x%5 == 0){
				total += x;
			}
			
		
		}
		
		System.out.println(total);

	}
	
	

}
