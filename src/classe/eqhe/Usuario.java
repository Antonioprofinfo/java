package classe.eqhe;

public class Usuario {
	
	
	
	// O hashcode ser� em outra aula.

	String nome;
	String email;

	@Override
	public boolean equals(Object objeto) {

		if (objeto instanceof Usuario) {
			Usuario outro = (Usuario) objeto;

			//return super.equals(objeto); // Primeiro forma
			
			//boolean nomeIgual = outro.nome == this.nome;
			//boolean emailIgual = outro.email == this.email; //Segunda forma
			
			boolean nomeIgual = outro.nome.equals(this.nome);
			boolean emailIgual = outro.email.equals(this.email);  //Terceira forma
			
			return nomeIgual && emailIgual;
			
		} else {
		return false;
		}
		

	}
	
	@Override
	public int hashCode() {
		return this.nome.length();		
	}
}


