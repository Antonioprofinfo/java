package classe.eqhe;

import java.util.Date;

@SuppressWarnings("unused")
public class EqualseHashcode {

	public static void main(String[] args) {
		
		Usuario u1 = new Usuario();		
		u1.nome = "Pedro Silva";
		u1.email = "pedro@123.com";
		

		Usuario u2 = new Usuario();		
		u2.nome = "Pedro Silva";
		u2.email = "pedro@123.com";
		
		System.out.println(u1 == u2); //False
		System.out.println(u1.equals(u2)); //False
		
		System.out.println(u1.nome == u2.nome);
		
		//System.out.println(u1.equals(new Date()));
		
	}

}
