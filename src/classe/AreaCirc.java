package classe;

public class AreaCirc {
	
	double raio;
	//static double pi = 3.14;
	static final double PI = 3.14;
	//double pi = 3.14;
	
	//Construtor
	AreaCirc(double raioInicial) {
		//PI = 3.14;
		raio = raioInicial;
	}
	
	//M�todo com o m�todo st�tico(Math.pow)
	double area() {
		//return pi * raio * raio;
		return PI * Math.pow(raio, 2);
	}
	
	static double area(double raio) {
		return PI * Math.pow(raio, 2);
	}
}
