package classe;

public class ValorVsReferencia {

	public static void main(String[] args) {
		
		double a = 2;
		double b = a; //Atribui��o por valor(Tipo Primitivo)
		a++;
		b--;
		System.out.printf("a = %.1f \nb = %.1f", a, b);
		
		DesafioData d1 = new DesafioData(1,6,2022);
		DesafioData d2 = d1; //Atribui��o por refer�ncia (Objeto)
		System.out.println("\n"+d2);
		
		//ambas as vari�veis est�o ligaos a mesma mem�ria.
		d1.dia = 21;
		d2.mes = 07;
		
		System.out.println(d1.obterDataFormatada());
		System.out.println(d2.obterDataFormatada());
		
		voltarDataParaValorPadrao(d1);
		
		System.out.println(d1.obterDataFormatada());
		System.out.println(d2.obterDataFormatada());
		
		int c = 5;
		alterarPrimitivo(c);
	    System.out.println(c);
				

	}
	
	//Objeto DesafioData � atributo no metodo voltar....
	//Recebemos a referencia em mem�ria.
	static void voltarDataParaValorPadrao(DesafioData d) {
		d.dia = 1;
		d.mes = 1;
		d.ano = 1979;
	}
	
	//O Objeto alterarPrimitivo � atributo por valor 
	//com utiliza��o de primitivo.
	static void alterarPrimitivo(int a) {
		a++;
	}

}
