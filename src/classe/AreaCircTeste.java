package classe;

public class AreaCircTeste {

	public static void main(String[] args) {
	
		AreaCirc a1 = new AreaCirc(10);
		//a1.pi = 10;
		//AreaCirc.pi = 5; /o PI foi tranformado em uma constante
		System.out.println(a1.area());
		System.out.println(AreaCirc.PI);
		
		
		AreaCirc a2 = new AreaCirc(5);
		//a2.pi = 5;
		//AreaCirc.pi = 10;	/o PI foi tranformado em uma constante	
		System.out.println(a2.area());
		System.out.println(AreaCirc.PI);
		
		//Acessando metodo estatico -> Classe.metodo()
		System.out.println(AreaCirc.area(10));
		
	}

}
