package classe;

public class DesafioData {
/*//Atributos - dia, mes e ano.
 * Instalciar no dataTeste
 * Trabalhando somnete com atributos
 * */
	
	int dia;
	int mes;
	int ano;
	
	//Contrutor padr�o
	//This pode ser um atributo ou m�todo.
	
	//byte, short, int, long -> 0 
	//float, double -> 0.0
	//boolean -> false ou true
	//char -> '\u0000'
	
	//Objetos possui falor null (N�o ocupa lugar de mem�ria).
	//String s -> null;
	
	
	
	DesafioData(){
		this(1, 1, 1970);  //this() Serve para chamar um contrutor
							// Dentro de outro construtor.
	}
	
	/*/Contrutor Explicito
	DesafioData(int diaItem, int mesItem, int anoItem){
		dia = diaItem;
		mes = diaItem;
		ano = anoItem;
	}
	*/
	
	//Contrutor Explicito this. -> serve para resolver nomes de atributos.
		DesafioData(int dia, int mes, int ano){
			this.dia = dia;
			this.mes = mes;
			this.ano = ano;
		}
	
	
	//Methodo -> obter data formatada
		
		String obterDataFormatada() {
			//String data = dia+"/"+mes+"/"+ano;
			//return data;
			String formato = "%d/%d/%d"; //Vari�vel local
			return String.format(formato, dia, mes, ano);
		}
		
		void imprimirDataFormatada() {
			System.out.println(this.obterDataFormatada());
		}
		

		
    
}
