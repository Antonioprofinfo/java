package classe;

public class DesafioDataTeste {

	public static void main(String[] args) {
		//Trabalhando somnete com atributos
		var d1 = new DesafioData(1, 1, 1970);
		var d2 = new DesafioData();
		d2.imprimirDataFormatada();
				
		//d1.dia = 19;
		//d1.mes = 07;
		//d1.ano = 1997;
		
		//System.out.println(d1.dia+"/"+d1.mes+"/"+d1.ano);
		
		System.out.println(d1.obterDataFormatada());
		
		d2.dia = 23;
		d2.mes = 02;
		d2.ano = 1997;
		
		//System.out.println(d2.dia+"/"+d2.mes+"/"+d2.ano);
		
		System.out.println(d2.obterDataFormatada());
		
		d2.imprimirDataFormatada();
	
		
		//int a;//erro
		int a  = 0;
		System.out.println(a); //� obrigat�rio inicializar
		
		String b = null;
		System.out.println(b); //� obrigat�rio inicializar
		
		final int x = 3; //� obrigat�rio referenciar e colocar o valor padr�o	
		System.out.println(x);
	}

}
