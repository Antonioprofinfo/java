package classe;

public class ValorNulo {

	public static void main(String[] args) {
		
		// Correto
		String s1 = "";
		System.out.println(s1.concat("!!!"));

		// Correto de compila��o
		// Erro n�o permite executar o programa
		//String s1;
		//System.out.println(s1.concat("!!!"));
		//resolu��o
		
		DesafioData d1 = Math.random() >0.5? new DesafioData() : null;
		
		if(d1 != null) {		
		System.out.println(d1.obterDataFormatada());
		}

		// Correto de rumtime
		// O compilador n�o entende algo.
		//String s2 = null; //Pois null n�o aponta para mem�ria.
		//System.out.println(s2.concat("!!!"));
		
		String s2 = Math.random() > 0.5 ? "Bom dia" : null; // Pois null n�o aponta para mem�ria.
		if (s2 != null) {
		System.out.println(s2.concat("!!!"));
		}
	}

}
