package classe;


//so h� necessidade de impota��o para classes fora do pacote.
public class ProdutoTeste {


	public static void main(String[] args) {
	
	//Contrutor com par�metro
	Produto p1 = new Produto("Notebook", 1000); //Inst�ncia  == Objeto
	//Atributos intancioados 
	//p1.preco = 2; //Valor pode substituir valor construtor padr�o.
	
	//Atributo st�tico
	Produto.desconto = 0.50;
	
	//Contrutor padr�a
	var p2 = new Produto(); //Inst�ncia  == Objeto
	//Atributos intancioados 
	p2.nome = "Caneta Preta";
	p2.preco = 100;
	//p2.desconto = 0.29;
	
	System.out.println(p1.nome +" "+ p1.precoComDesconto());
	System.out.println(p2.nome +" "+ p2.precoComDesconto());
	
	//Methodo implementado 
	//Metodos Diferente com mesmo nome
	double precoFinal1 = p1.precoComDesconto();
	System.out.println(precoFinal1);
	double precoFinal2 = p2.precoComDesconto();
	System.out.println(precoFinal2);
	
	//Apenas atributos
	//double precoFinal1 = p1.preco * (1 - p1.desconto);
	//double precoFinal2 = p2.preco * (1 - p2.desconto);
	
	double mediaCarrinho =(precoFinal1 + precoFinal2) / 2;
	
	System.out.printf("M�dia � R$%.2f", mediaCarrinho);
	
	/*var desc = new Produto();
	desc.preco = 100;
	desc.desconto = 0.1;
	double desc1 = desc.precoComDesconto();
	System.out.println("\n"+ desc1);
	*/
	
	}

}
