package classe;

public class Produto {
	
	//Atributos
	String nome;
	double preco;
	static double desconto = 0.25;
	
	Produto() {

	}
	
	Produto(String nomeItem) {
		nome = nomeItem;
	}
	
	Produto(String nomeItem, double precoInicial) {
		nome = nomeItem;
		preco = precoInicial;
	}

	
	
	/*double precoComDesconto(double valor, double desc) {
		return (valor * (1 - desc));
		}
	 * 
	 * */
	double precoComDesconto() {
		return (preco * (1 - desconto));
	}
	
	double precoComDesconto(double descontoExtra) {
		return (preco * (1 - desconto + descontoExtra));
	}
	
		
}
