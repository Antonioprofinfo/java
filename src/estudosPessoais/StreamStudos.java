package estudosPessoais;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
//import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class StreamStudos {

	public static void main(String[] args) {
		
		Consumer<String> print = System.out::println;
		//Consumer<Integer> println = System.out::println;
		
		List<String> transporte = Arrays.asList("Carro", "Aviao", "Onibus");
		
		String[] usuario = {"Antonio", "Gabriel", "Flavia"};
		
		int[] numero = {1,2,3,4,5};
		
		for(Integer n : numero) {
			System.out.println(n+1);
		}
		
		UnaryOperator<String> maiscula = m ->  m + "!";
		//UnaryOperator<Integer> soma = m ->  m + 1;
		
		//Acesso por lista
		transporte.stream()
				.map(maiscula)
				.forEach(print);
		
		//numero.stream()
				//.map(soma)
				//.forEach(println);
		
		//Acesso por variável
		Stream.of(usuario)
					.map(maiscula)
					.forEach(print);
		
		//Stream.of(numero)
					//.map(soma)
					//.forEach(println);
		
		 
		
	}

}
