package estudosPessoais;

import java.util.Arrays;
import java.util.List;
//import java.util.function.Consumer;

public class DadosAcesso {
	
	  

	public static void main(String[] args) {
		Produto p1 = new Produto(01, "Cadeira", 100, 30);
		Produto p2 = new Produto(01, "Mesa", 150, 20);
		Produto p3 = new Produto(02, "Sofá", 150, 20);
		
		List<Produto> produtos = Arrays.asList(p1, p2, p3);
		
		produtos.stream()
			.filter(a -> a.valor <= 150)
			.filter(a -> a.nomeProduto == "Cadeira")
			.map(a -> 
				a.codproduto +" "+ 
				//a.nomeProduto+" "+ 
				//a.quantidadeProduto+" "+
				a.valor)
			.forEach(System.out::println);
		
		
		

	}

}
